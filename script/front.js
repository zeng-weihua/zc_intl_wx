require.config({
	paths: {
		"jquery": "lib/jquery", //jquery包
		"skitter": "jquery.skitter.min",
		"easing": "jquery.easing.1.3",
		"fancyBox": "fancyBox/jquery.fancybox",
		"thumbs": "fancyBox/helpers/jquery.fancybox-thumbs",
		'Jcrop': "jquery.Jcrop.min",
		'animated':'animated', // 动画库
		'fullPage':'jquery.fullPage' // 滚屏包
	},
	shim: {
		easing: {
			deps: ['jquery']
		},
		skitter: {
			deps: ['easing'],
			exports: 'skitter'
		},
		fancyBox: {
			deps: ["jquery"],
			exports: "window,document,jQuery"
		},
		thumbs: {
			deps: ["jquery","fancyBox"],
			exports: "window,document,jQuery"
		},
		Jcrop: {
			deps: ["jquery"],
			exports: "jQuery"
		},
		animated:{
			deps:['jquery'],
			exports: 'animated'
		},
		fullPage:{
			deps:['jquery'],
			exports:'fullPage'
		}
	}
});
define(['jquery','dialog','animated','fullPage'],function($){
	//选项卡
	var tabcard = function(menu,cont){
			menu.eq(0).addClass('click');
			cont.hide().eq(0).show();
			menu.click(function(){
				var index = $(this).index();
				$(this).addClass('click').siblings().removeClass('click');
				cont.hide().eq(index).show();
			});
		}
	tabcard($('.detail-menu').children(),$('.detail'));

	return {
		// 为当前页面的导航添加选中后的样式
		navSelected: function(index){
			$(".zc_nav_ul>li").eq(index).addClass("curr");
		}
	};
});
